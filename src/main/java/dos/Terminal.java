package dos;
import java.io.*;
import java.util.*;

// import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;


public class Terminal extends javax.swing.JFrame {

    private String feedback;

    public Terminal() 
    {
        initComponents();
    }

    public Prompt PromptParser(String input)
    {
        String[] parsing = input.trim().split("\\s+");
        switch(parsing.length)
        {
            case 1:
                return new Prompt(parsing[0]);
            case 2:
                return new Prompt(parsing[0], parsing[1]);
            case 3:
                return new Prompt(parsing[0], parsing[1], parsing[2]);
            default:
                return new Prompt();       
        }   
    }

    public String GetRootDirectory()
    {
        File directory = new File(System.getProperty("user.dir"));
        directory = directory.getParentFile();
        return directory.getAbsolutePath() + "/";
    }

    public void CreateDirectory(String name)
    {
        String sourceDirectory = GetRootDirectory();
        File newDirectory = new File(sourceDirectory + name);
        boolean isCreated = newDirectory.mkdir();
        feedback = isCreated ? "- SUCCESS" : "- ERROR";
    }

    public void CreateFile(String name)
    {
        try {
            String sourceDirectory = GetRootDirectory();
            File newFile = new File(sourceDirectory + name);
            BufferedWriter writer = new BufferedWriter(new FileWriter(newFile));
            boolean isCreated = newFile.isFile();
            feedback = isCreated ? "- SUCCESS" : "- ERROR";      
            writer.close();
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    public void DeletedFile(Prompt prompt)
    {
        String sourceDirectory = GetRootDirectory();
        File targetFile = new File(sourceDirectory + prompt.source);
        boolean isDeleted = targetFile.delete();
        feedback = isDeleted ? "- FILE DELETED" : "- ERROR";   
    }

    public void CopyFile(Prompt prompt)
    {
        String sourceDirectory = GetRootDirectory();
        File sourceFile = new File(sourceDirectory + prompt.source);
        File targetFile = new File(sourceDirectory + prompt.destination);

        try {
            FileInputStream source = new FileInputStream(sourceFile);
            FileOutputStream destination = new FileOutputStream(targetFile);

            byte[] buffer = new byte[4096];
            int byteRead;

            while((byteRead = source.read(buffer))!=-1)
                destination.write(buffer, 0, byteRead);

            source.close();
            destination.close();

            Log("SUCCESS");
        } catch (Exception e) {
            e.getStackTrace();
        }
            
    }

    public void Listing()
    {
        String sourceDirectory = GetRootDirectory();
        File directory = new File(sourceDirectory);
        if(directory.isDirectory())
        {
            File[] fileList = directory.listFiles();
            for (File file : fileList) {
                Log(file.getName());
            }
        }
    }

    public void Finding(Prompt prompt)
    {
        int count = 0;
        String sourceDirectory = GetRootDirectory();
        File directory = new File(sourceDirectory);
        if(directory.isDirectory())
        {
            File[] fileList = directory.listFiles();
            for (File file : fileList) 
            {
                if(file.getName().contains(prompt.source))
                {
                    Log(file.getName());
                    count++;
                }

            }
        }

        if(count < 1)
            Log(prompt.source + " not found");
    }

    private String commitTarget;
    public void WriteFile(Prompt prompt)
    {
        
        ClearLog();
        ReadFile(prompt);
        commitTarget = GetRootDirectory() + prompt.source;
    }

    public void Commit(String path)
    {
        String content = feedbackTextArea.getText();
        File source = new File(path);

        if(source.isFile())
        {
            try {
                BufferedWriter writer = new BufferedWriter(new FileWriter(source));
                writer.write(content);
                writer.close();
                
            } catch (Exception e) {
                //TODO: handle exception
            }
        }
        commitTarget = "";

    }

    public void ReadFile(Prompt prompt)
    {
        String sourceDirectory = GetRootDirectory();
        File source = new File(sourceDirectory + prompt.source);

        if(source.isFile())
        {
            try {
                FileReader file = new FileReader(sourceDirectory + prompt.source);
                BufferedReader reader = new BufferedReader(file);
                String output;

                while((output = reader.readLine()) != null)
                {
                    Log(output);
                }
                reader.close();
            } catch (Exception e) {
                e.getStackTrace();
            }
        }
        else
        {
            Log("file not found");
        }
    }

    public void RenameFile(Prompt prompt)
    {
        String sourceDirectory = GetRootDirectory();
        File oldfile =new File(sourceDirectory + prompt.source);
        File newfile =new File(sourceDirectory + prompt.destination);

        if(oldfile.isFile())
        {
            Log(prompt.fullPrompt);
            if(oldfile.renameTo(newfile))
                Log("- SUCCESS");
            else
                Log("- ERROR");
        }
        
    }

    public void LastModified(Prompt prompt)
    {
        String sourceDirectory = GetRootDirectory();
        File source = new File(sourceDirectory + prompt.source);
        Date date = new Date(source.lastModified());
        Log(source.getName() + ": last modified : " + date.toString());
    }


    public void FileProperties(Prompt prompt)
    {
        String sourceDirectory = GetRootDirectory();
        File source = new File(sourceDirectory + prompt.source);
        String readable = source.canRead() ? "R" : "-";
        String writeable = source.canRead() ? "W" : "-";
        String executeable = source.getName().contains(".sh") ? 
            "E" : "-" ;

        Log("R/W/E : " + readable + "/" + writeable + "/" + executeable);
    }


    public void Execute(Prompt prompt)
    {
        if(prompt.command.equals("mkdir"))
        {
            CreateDirectory(prompt.source);
            Log(prompt.fullPrompt, feedback);
        }
        else if(prompt.command.equals("touch"))
        {
            CreateFile(prompt.source);
            Log(prompt.fullPrompt, feedback);
        }
        else if(prompt.command.equals("ls"))
        {
            Listing();
        }
        else if(prompt.command.equals("grep"))
        {
            Finding(prompt);
        }
        else if(prompt.command.equals("clear"))
        {
            ClearLog();
        }
        else if(prompt.command.equals("cat"))
        {
            ReadFile(prompt);
        }
        else if(prompt.command.equals("nano"))
        {
            WriteFile(prompt);
        }
        else if(prompt.command.equals("mv"))
        {
            RenameFile(prompt);
        }
        else if(prompt.command.equals("cp"))
        {
            CopyFile(prompt);
        }
        else if(prompt.command.equals("rm"))
        {
            DeletedFile(prompt);
        }
        else if(prompt.command.equals("-l"))
        {
            FileProperties(prompt);
        }
        else if(prompt.command.equals("-m"))
        {
            LastModified(prompt);
        }

        else if(prompt.command.equals("help"))
        {
            Help();
        }
        else
        {
            Log(prompt.fullPrompt);
        }
    }

    public void Help()
    {
        Log("mkdir <new-directory>");
        Log("touch <new-file>");
        Log("ls");
        Log("grep <string-name>");
        Log("clear");
        Log("cat <file-name>");
        Log("nano <file-name>");
        Log("mv <old-name> <new-name>");
        Log("rm <file-name>");
        Log("-l <file-name>");
        Log("-m <file-name>");
    }



    private void commandTextFieldKeyPressed(java.awt.event.KeyEvent evt) 
    {
        if(evt.getKeyCode() == KeyEvent.VK_ENTER)
        {
            String input = promptTextField.getText();
            Prompt prompt = PromptParser(input);

            Execute(prompt);

        }

    }

    // COMMIT
    private void feedbackTextAreaKeyPressed(java.awt.event.KeyEvent evt) 
    {
        // boolean enter = evt.getKeyCode() == KeyEvent.VK_ENTER;
        boolean escape = evt.getKeyCode() == KeyEvent.VK_ESCAPE;
        if(escape)
        {
            Commit(commitTarget);
            ClearLog();
            Log("file saved");
        }

    }

    private void commandTextFieldActionPerformed(java.awt.event.ActionEvent evt) 
    { }

    private void Log(String log) {

        feedbackTextArea.setText( 
            feedbackTextArea.getText().isEmpty() ? 
            log : feedbackTextArea.getText() + "\n" + log);
        promptTextField.setText("");

    }

    private void Log(String log, String feedback) {

        feedbackTextArea.setText( 
            feedbackTextArea.getText().isEmpty() ? 
            log + " " + feedback : feedbackTextArea.getText() + "\n" + log + " " + feedback);
        promptTextField.setText("");

    }

    private void ClearLog()
    {
        feedbackTextArea.setText("");
        promptTextField.setText("");
    }


    //region Stuff
    private void initComponents() {

        promptTextField = new javax.swing.JTextField();
        scrollPane = new javax.swing.JScrollPane();
        feedbackTextArea = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        promptTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                commandTextFieldActionPerformed(evt);
            }
        });
        promptTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                commandTextFieldKeyPressed(evt);
            }
        });


        feedbackTextArea.setColumns(20);
        feedbackTextArea.setRows(5);
        feedbackTextArea.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                feedbackTextAreaKeyPressed(evt);
            }
        });

        feedbackTextArea.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                commandTextFieldKeyPressed(evt);
            }
        });
        scrollPane.setViewportView(feedbackTextArea);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(promptTextField)
                    .addComponent(scrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 376, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(promptTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(scrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 235, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents




    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Terminal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Terminal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Terminal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Terminal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Terminal().setVisible(true);
            }
        });
        
    }

    private javax.swing.JTextField promptTextField;
    private javax.swing.JTextArea feedbackTextArea;
    private javax.swing.JScrollPane scrollPane;
    //endregion
}